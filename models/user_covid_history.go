package models

import (
	"time"

	"github.com/go-ozzo/ozzo-validation/v4"
)

// UserCovidHistory Struct validations
type UserCovidHistory struct {
	ID          uint64    `gorm:"primary_key;auto_increment" json:"id"`
	UserID      uint64    `json:"user_id"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	CovidStatus int32     `gorm:"size:10" json:"covid_status"`
}

// Prepare trim data and set default data
func (uch *UserCovidHistory) Prepare() {
	uch.ID = 0

	uch.CreatedAt = time.Now()
	uch.UpdatedAt = time.Now()
}

// Validate is function to validate model
func (uch UserCovidHistory) Validate() error {
	return validation.ValidateStruct(&uch,
		validation.Field(&uch.CovidStatus, validation.Required),
		validation.Field(&uch.UserID, validation.Required),
	)
}
