package models

import (
	"html"
	"strings"
	"time"

	"github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"golang.org/x/crypto/bcrypt"
)

// User Struct validations
type User struct {
	ID          uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Email       string    `gorm:"size:100;not null;unique" json:"email"`
	Password    string    `gorm:"size:100;" json:"password"`
	FullName    string    `gorm:"size:255;not null" json:"full_name"`
	Phone       string    `gorm:"size:100;" json:"phone"`
	Birthday    time.Time `json:"birthday"`
	Gender      int32     `gorm:"size:10" json:"gender"`
	Nationality string    `gorm:"size:100;" json:"nationality"`
	GoogleID    string    `gorm:"size:100;index" json:"google_id"`
	FacebookID  string    `gorm:"size:100;index" json:"facebook_id"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	CovidStatus int32     `gorm:"size:10" json:"covid_status"`

	UserCovidHistories []UserCovidHistory `gorm:"foreignkey:UserId"`
}

// Hash should be hashing password input
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// VerifyPassword should be compare save password with input password weight
func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

// BeforeSave hashing password
func (u *User) BeforeSave() error {
	hashedPassword, err := Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

// Prepare trim data and set default data
func (u *User) Prepare() {
	u.ID = 0
	u.FullName = html.EscapeString(strings.TrimSpace(u.FullName))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
	u.Nationality = html.EscapeString(strings.TrimSpace(u.Nationality))

	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

// Validate is function to validate model
func (u User) Validate(action string) error {
	switch strings.ToLower(action) {
	case "update":
		return validation.ValidateStruct(&u,
			validation.Field(&u.FullName, validation.Required),
			validation.Field(&u.Password, validation.Required),
			validation.Field(&u.Gender, validation.Required),
			validation.Field(&u.Birthday, validation.Required),
			validation.Field(&u.Nationality, validation.Required),
			validation.Field(&u.Email, validation.Required, is.Email),
		)
	case "login":
		return validation.ValidateStruct(&u,
			validation.Field(&u.FullName, validation.Required),
			validation.Field(&u.Password, validation.Required),
			validation.Field(&u.Email, validation.Required, is.Email),
		)
	default:
		return validation.ValidateStruct(&u,
			validation.Field(&u.FullName, validation.Required),
			validation.Field(&u.Email, validation.Required, is.Email),
		)
	}
}
