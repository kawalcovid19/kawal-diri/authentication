package accessor

import (
	"gitlab.com/kawalcovid19/kawal-diri/authentication/models"
)

// Accessor is an accessor of a data source (e.g. database).
type Accessor interface {
	GetUserByEmail(email string) (models.User, error)
	GetUserByID(id uint64) (models.User, error)
	GetUserByGoogleID(googleID string) (models.User, error)
	GetUserByFacebookID(facebookID string) (models.User, error)
	SaveUser(user *models.User) error
	UpdateUser(id uint64, user *models.User) error
	UpdateUserAndUserCovidHistory(id uint64, user *models.User) error
	SaveUserCovidHistory(userCovidHistory *models.UserCovidHistory) error
}
