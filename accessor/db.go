package accessor

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"gitlab.com/kawalcovid19/kawal-diri/authentication/models"
)

type dbAccessor struct {
	db *gorm.DB
}

var (
	ErrorNotFound = errors.New("record not found")
)

// NewDBAccessor create a new DB accessor.
func NewDBAccessor(db *gorm.DB) (Accessor, error) {
	if err := db.DB().Ping(); err != nil {
		return nil, errors.Wrap(err, "check connection")
	}

	return &dbAccessor{db: db}, nil
}

func (d *dbAccessor) GetUserByEmail(email string) (models.User, error) {
	var user models.User
	err := d.db.Where("email = ?", email).First(&user).Error

	if gorm.IsRecordNotFoundError(err) {
		return user, ErrorNotFound
	}

	return user, err
}

func (d *dbAccessor) GetUserByID(id uint64) (models.User, error) {
	var user models.User
	err := d.db.Where("id = ?", id).First(&user).Error

	if gorm.IsRecordNotFoundError(err) {
		return user, ErrorNotFound
	}
	return user, err
}

func (d *dbAccessor) GetUserByGoogleID(googleID string) (models.User, error) {
	var user models.User
	err := d.db.Where("google_id = ?", googleID).First(&user).Error

	if gorm.IsRecordNotFoundError(err) {
		return user, ErrorNotFound
	}

	return user, err
}

func (d *dbAccessor) GetUserByFacebookID(facebookID string) (models.User, error) {
	var user models.User
	err := d.db.Where("facebook_id = ?", facebookID).First(&user).Error

	if gorm.IsRecordNotFoundError(err) {
		return user, ErrorNotFound
	}

	return user, err
}

// SaveUser to DB
func (d *dbAccessor) SaveUser(user *models.User) error {
	return d.db.Create(user).Error
}

// UpdateUser only updates modified fields
func (d *dbAccessor) UpdateUser(id uint64, user *models.User) error {
	u := models.User{ID: id}
	if err := d.db.Model(&u).Updates(*user).Error; err != nil {
		return err
	}

	*user = u
	return nil
}

func (d *dbAccessor) SaveUserCovidHistory(userCovidHistory *models.UserCovidHistory) error {
	return d.db.Create(userCovidHistory).Error
}

func (d *dbAccessor) UpdateUserAndUserCovidHistory(id uint64, user *models.User) error {
	if err := d.UpdateUser(id, user); err != nil {
		return err
	}

	uch := models.UserCovidHistory{UserID: user.ID, CovidStatus: user.CovidStatus}
	if err := d.SaveUserCovidHistory(&uch); err != nil {
		return err
	}

	return nil
}
