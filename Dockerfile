FROM golang:latest AS builder

RUN mkdir /app
WORKDIR /app
COPY . .

# build for linux
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/authentication_server cmd/server/*.go

# multistage build compress file size
FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder ./app/bin .
COPY --from=builder ./app/statik .
COPY --from=builder ./app/swaggerui .
COPY --from=builder ./app/.env.example ./.env

ENV DB_HOST '' \
    DB_DRIVER '' \
    DB_USER '' \
    DB_PASSWORD '' \
    DB_NAME '' \
    DB_PORT '' \
    HTTP_PORT '' \
    GRPC_PORT '' \
    API_SECRET '' \
    SWAGGER_PORT '' \
    SERVE_SWAGGER ''

CMD ["./authentication_server"]  
