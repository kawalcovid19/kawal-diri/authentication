package auth

import (
	"errors"
	"math"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// CreateToken generate JWT Token base on user_id
func CreateToken(userID uint64) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["user_id"] = userID
	claims["exp"] = time.Now().Add(time.Hour * 1).Unix() //Token expires after 1 hour
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(os.Getenv("API_SECRET")))

}
func ExtractToken(tokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{},
		error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok ||
			method != jwt.SigningMethodHS256 {
			return nil, errors.New("Signing method invalid")
		}
		return []byte(os.Getenv("API_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("Invalid token")
	}
	sec, dec := math.Modf(claims["exp"].(float64))
	exp := time.Unix(int64(sec), int64(dec*(1e9)))
	if exp.Before(time.Now()) {
		return nil, errors.New("Token expired")
	}
	return claims, nil
}
