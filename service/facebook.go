package service

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	service_proto "gitlab.com/kawalcovid19/kawal-diri/authentication/service/proto"
)

// FacebookUser is struct user from facebook login
type facebookUser struct {
	Email       string        `json:"email"`
	Name        string        `json:"name"`
	FirstName   string        `json:"first_name"`
	LastName    string        `json:"last_name"`
	ID          string        `json:"id"`
	Error       facebookError `json:"error"`
	AccessToken string        `json:"access_token"`
}

type facebookError struct {
	Message string `json:"message"`
}

func facebookUserByAccessToken(accessToken string) (facebookUser, error) {
	var user facebookUser
	req, err := http.NewRequest("GET", "https://graph.facebook.com/v5.0/me", nil)
	if err != nil {
		return user, errors.New("failed initialized http request to facebook")
	}

	q := req.URL.Query()
	q.Add("access_token", accessToken)
	q.Add("fields", "id,last_name,email,first_name,name")

	req.URL.RawQuery = q.Encode()

	resp, err := http.Get(req.URL.String())

	if err != nil {
		return user, err
	}

	bytesBody, _ := ioutil.ReadAll(resp.Body)
	if err != nil {
		return user, err
	}

	if err := json.Unmarshal(bytesBody, &user); err != nil {
		return user, err
	}

	if (facebookError{}) != user.Error {
		return user, errors.New(user.Error.Message)
	}

	user.AccessToken = accessToken
	return user, nil
}

func facebookUserToProtoUser(fbUser facebookUser) *service_proto.User {
	return &service_proto.User{
		Email:               fbUser.Email,
		FullName:            fbUser.Name,
		FacebookAccessToken: fbUser.AccessToken,
	}
}
