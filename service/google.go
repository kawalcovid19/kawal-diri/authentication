package service

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	service_proto "gitlab.com/kawalcovid19/kawal-diri/authentication/service/proto"
)

// GoogleUser is struct user from google login
type googleUser struct {
	Email         string `json:"email"`
	EmailVerified string `json:"email_verified"`
	FamilyName    string `json:"family_name"`
	GivenName     string `json:"given_name"`
	Name          string `json:"name"`
	Picture       string `json:"picture"`
	Sub           string `json:"sub"`
	Error         string `json:"error"`
	RequestID     string `json:"request_id"`
}

func googleUserByIDToken(idToken string) (googleUser, error) {
	var user googleUser
	req, err := http.NewRequest("GET", "https://oauth2.googleapis.com/tokeninfo", nil)
	if err != nil {
		return user, errors.New("failed initializing http request to google")
	}

	q := req.URL.Query()
	q.Add("id_token", idToken)
	req.URL.RawQuery = q.Encode()

	resp, err := http.Get(req.URL.String())
	if err != nil {
		return user, err
	}

	bytesBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return user, err
	}

	if err := json.Unmarshal(bytesBody, &user); err != nil {
		return user, err
	}

	if user.Error != "" {
		return user, errors.New(user.Error)
	}

	user.RequestID = idToken
	return user, nil
}

func googleUserToProtoUser(gUser googleUser) *service_proto.User {
	return &service_proto.User{
		Email:           gUser.Email,
		FullName:        gUser.Name,
		GoogleRequestId: gUser.RequestID,
	}
}
