package service

import (
	"context"
	"time"

	"gitlab.com/kawalcovid19/kawal-diri/authentication/accessor"
	"gitlab.com/kawalcovid19/kawal-diri/authentication/auth"
	"gitlab.com/kawalcovid19/kawal-diri/authentication/models"
	service_proto "gitlab.com/kawalcovid19/kawal-diri/authentication/service/proto"
)

type authService struct {
	ac accessor.Accessor
}

const (
	layoutISO = "2006-01-02"
	layoutUS  = "January 2, 2006"
)

// NewAuthService create a new auth service.
func NewAuthService(ac accessor.Accessor) (service_proto.AuthenticationServer, error) {
	return &authService{ac: ac}, nil
}

func (a *authService) LoginByGoogle(_ context.Context, req *service_proto.LoginByGoogleRequest) (*service_proto.LoginResponse, error) {
	gUser, err := googleUserByIDToken(req.RequestId)
	if err != nil {
		return &service_proto.LoginResponse{ResponseCode: service_proto.LoginResponse_THIRD_PARTY_ERROR}, err
	}

	user, err := a.ac.GetUserByGoogleID(gUser.Sub)
	if err != nil {
		if err == accessor.ErrorNotFound {
			user, err = a.ac.GetUserByEmail(gUser.Email)
			if err != nil {
				if err == accessor.ErrorNotFound {
					loginResponse := &service_proto.LoginResponse{
						ResponseCode:   service_proto.LoginResponse_SUCCESS,
						User:           googleUserToProtoUser(gUser),
						RegisteredUser: service_proto.LoginResponse_UNREGISTER,
					}

					return loginResponse, nil
				} else {
					loginResponse := &service_proto.LoginResponse{
						ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR,
					}

					return loginResponse, err
				}
			}

			user.GoogleID = gUser.Sub
			if err := a.ac.UpdateUser(user.ID, &user); err != nil {
				loginResponse := &service_proto.LoginResponse{
					ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR,
				}

				return loginResponse, err
			}
		} else {
			loginResponse := &service_proto.LoginResponse{
				ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR,
			}

			return loginResponse, err
		}
	}

	token, err := auth.CreateToken(user.ID)
	if err != nil {
		return &service_proto.LoginResponse{ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR}, err
	}

	loginResponse := &service_proto.LoginResponse{
		ResponseCode:   service_proto.LoginResponse_SUCCESS,
		User:           userModelToProtoUser(user),
		RegisteredUser: service_proto.LoginResponse_REGISTER,
		Token:          token,
	}

	return loginResponse, nil
}

func (a *authService) LoginByFacebook(_ context.Context, req *service_proto.LoginByFacebookRequest) (*service_proto.LoginResponse, error) {
	fbUser, err := facebookUserByAccessToken(req.AccessToken)
	if err != nil {
		return &service_proto.LoginResponse{ResponseCode: service_proto.LoginResponse_THIRD_PARTY_ERROR}, err
	}

	user, err := a.ac.GetUserByFacebookID(fbUser.ID)
	if err != nil {
		if err == accessor.ErrorNotFound {
			user, err = a.ac.GetUserByEmail(fbUser.Email)
			if err != nil {
				if err == accessor.ErrorNotFound {
					loginResponse := &service_proto.LoginResponse{
						ResponseCode:   service_proto.LoginResponse_SUCCESS,
						User:           facebookUserToProtoUser(fbUser),
						RegisteredUser: service_proto.LoginResponse_UNREGISTER,
					}

					return loginResponse, nil
				} else {
					loginResponse := &service_proto.LoginResponse{
						ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR,
					}

					return loginResponse, err
				}
			}

			user.FacebookID = fbUser.ID
			if err := a.ac.UpdateUser(user.ID, &user); err != nil {
				loginResponse := &service_proto.LoginResponse{
					ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR,
				}

				return loginResponse, err
			}
		} else {
			loginResponse := &service_proto.LoginResponse{
				ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR,
			}

			return loginResponse, err
		}
	}

	token, err := auth.CreateToken(user.ID)
	if err != nil {
		return &service_proto.LoginResponse{ResponseCode: service_proto.LoginResponse_INTERNAL_SERVER_ERROR}, err
	}

	loginResponse := &service_proto.LoginResponse{
		ResponseCode:   service_proto.LoginResponse_SUCCESS,
		User:           userModelToProtoUser(user),
		RegisteredUser: service_proto.LoginResponse_REGISTER,
		Token:          token,
	}

	return loginResponse, nil
}

func (a *authService) SignUp(_ context.Context, protoUser *service_proto.User) (*service_proto.SignUpResponse, error) {
	birthday, err := time.Parse(layoutISO, protoUser.GetBirthday())
	if err != nil {
		return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_VALIDATION_ERROR}, err
	}

	var gUser googleUser
	if protoUser.GetGoogleRequestId() != "" {
		gUser, err = googleUserByIDToken(protoUser.GetGoogleRequestId())
		if err != nil {
			return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_VALIDATION_ERROR}, err
		}

		_, err := a.ac.GetUserByGoogleID(gUser.Sub)
		if err != accessor.ErrorNotFound {
			if err != nil {
				return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_INTERNAL_SERVER_ERROR}, err
			}

			return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_VALIDATION_ERROR}, err
		}
	}

	var fbUser facebookUser
	if protoUser.GetFacebookAccessToken() != "" {
		fbUser, err = facebookUserByAccessToken(protoUser.GetFacebookAccessToken())
		if err != nil {
			return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_VALIDATION_ERROR}, err
		}

		_, err := a.ac.GetUserByFacebookID(fbUser.ID)
		if err != accessor.ErrorNotFound {
			if err != nil {
				return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_INTERNAL_SERVER_ERROR}, err
			}

			return &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_VALIDATION_ERROR}, err
		}
	}

	user := models.User{
		Email:       protoUser.GetEmail(),
		Password:    protoUser.GetPassword(),
		FullName:    protoUser.GetFullName(),
		Phone:       protoUser.GetPhone(),
		Birthday:    birthday,
		Gender:      int32(protoUser.GetGender()),
		Nationality: protoUser.GetNationality(),
		CovidStatus: int32(protoUser.GetCovidStatus()),
	}

	if gUser != (googleUser{}) {
		user.GoogleID = gUser.Sub
	}

	if fbUser != (facebookUser{}) {
		user.FacebookID = fbUser.ID
	}

	if protoUser.CovidStatus == service_proto.CovidStatus_BLANK {
		user.CovidStatus = int32(service_proto.CovidStatus_UNKNOWN)
	}

	err = user.Validate("")
	if err != nil {
		response := &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_VALIDATION_ERROR}
		return response, err
	}

	if err := a.ac.SaveUser(&user); err != nil {
		response := &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_INTERNAL_SERVER_ERROR}
		return response, err
	}

	token, err := auth.CreateToken(user.ID)
	if err != nil {
		response := &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_INTERNAL_SERVER_ERROR}
		return response, err
	}

	response := &service_proto.SignUpResponse{ResponseCode: service_proto.SignUpResponse_SUCCESS, Token: token}
	return response, nil
}

func (a *authService) GetUser(_ context.Context, protoGetUserRequest *service_proto.GetUserRequest) (*service_proto.GetUserResponse, error) {
	claims, err := auth.ExtractToken(protoGetUserRequest.GetToken())
	if err != nil {
		return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_INVALID_TOKEN}, err
	}

	user, err := a.ac.GetUserByID(uint64(claims["user_id"].(float64)))
	if err != nil {
		return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_INVALID_TOKEN}, err
	}

	protoUser := userModelToProtoUser(user)
	return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_SUCCESS, User: protoUser}, nil
}

func (a *authService) UpdateCovidStatus(_ context.Context, protoCovidStatusRequest *service_proto.CovidStatusRequest) (*service_proto.GetUserResponse, error) {
	claims, err := auth.ExtractToken(protoCovidStatusRequest.GetToken())
	if err != nil {
		return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_INVALID_TOKEN}, err
	}

	user, err := a.ac.GetUserByID(uint64(claims["user_id"].(float64)))
	if err != nil {
		return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_INVALID_TOKEN}, err
	}

	user.CovidStatus = int32(protoCovidStatusRequest.GetCovidStatus())
	if err := a.ac.UpdateUserAndUserCovidHistory(user.ID, &user); err != nil {
		return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_INTERNAL_SERVER_ERROR}, err
	}

	protoUser := userModelToProtoUser(user)
	return &service_proto.GetUserResponse{ResponseCode: service_proto.GetUserResponse_SUCCESS, User: protoUser}, nil
}

func userModelToProtoUser(user models.User) *service_proto.User {
	return &service_proto.User{
		Id:          user.ID,
		Email:       user.Email,
		FullName:    user.FullName,
		Phone:       user.Phone,
		Birthday:    user.Birthday.Format(layoutISO),
		Gender:      service_proto.Gender(user.Gender),
		Nationality: user.Nationality,
		CovidStatus: service_proto.CovidStatus(user.CovidStatus),
	}
}
