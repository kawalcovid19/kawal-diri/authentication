export GO111MODULE=on
help: ## This help dialog.
help h:
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
	printf "%-20s %s\n" "target" "help" ; \
	printf "%-20s %s\n" "------" "----" ; \
	for help_line in $${help_lines[@]}; do \
		IFS=$$':' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf '\033[36m'; \
		printf "%-20s %s" $$help_command ; \
		printf '\033[0m'; \
		printf "%s\n" $$help_info; \
	done

# def
GO = go
BIN_PATH = bin
GOFLAGS = -p 4 -v
BUILD = $(GO) build $(GOFLAGS) -o
DOWNLOAD_DEPENDENCY = $(GO) mod download

server_build_path = cmd/server

# build
COMPONENT = $@
# this variable releated to the Jenkins run part
BIN_ALIAS = $(COMPONENT)
BIN = $(BIN_PATH)/$(BIN_ALIAS) # binary name
BUILD_PATH = $($(COMPONENT)_build_path)

all: ## Make all components
all a: server

server: ## Build server

server: dependency
	@mkdir -p $(BIN_PATH)
	$(BUILD) $(BIN) $(BUILD_PATH)/*.go

.PHONY: clean
clean: ## clean executables
	rm -rf $(BIN_PATH)/*

.PHONY: dependency
dependency: ## download dependency
	$(DOWNLOAD_DEPENDENCY)

.PHONY: vet
vet: ## go vet files
	@$(GO) vet $(GOFLAGS) ./...

.PHONY: generate
generate:
	# Generate go, gRPC-Gateway, swagger output.
	#
	# -I declares import folders, in order of importance
	# This is how proto resolves the protofile imports.
	# It will check for the protofile relative to each of these
	# folders and use the first one it finds.
	#
	# --go_out generates go Protobuf output with gRPC plugin enabled.
	# 		paths=source_relative means the file should be generated
	# 		relative to the input proto file.
	# --grpc-gateway_out generates gRPC-Gateway output.
	# --swagger_out generates an OpenAPI 2.0 specification for our gRPC-Gateway endpoints.
	#
	# service/proto/auth.proto is the location of the protofile we use.
	protoc \
		-I service/proto \
		-I third-party \
		--go_out=plugins=grpc,paths=source_relative:./service/proto \
		--grpc-gateway_out=./service/proto \
		--swagger_out=allow_merge=true,merge_file_name=authentication:./swaggerui \
		service/proto/auth.proto

	statik -m -f -src swaggerui
	
